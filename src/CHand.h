//----------------------------------------------------------------------------------------
/**
 * \file       CHand.h
 * \author     Lukas Sedlacek (sedlalu2)
 * \date       2015/02/25
 * \brief      Hand representer  
 *
 * Class represents a hand detected by a sensor. There are two types of the tracked hand - mouse and gesture hand (for more information \see Common.h). 
 * Class has also implemented click gesture detection.
*/
//----------------------------------------------------------------------------------------
#ifndef _CHAND_H_SEDLALU2
#define _CHAND_H_SEDLALU2

#include "NiTE.h"
#include "TBoundingBox.h"

/// Hand representer 
/**
	This class represents one hand detected by OpenNI2 algorithm. It stores all information about its position, id, bounding box and type. When 
	hand is detected then also initial position has stored (this position is lately used for gesture detection).
*/
class CHand { 

	nite::HandId		m_Id; 				///< Hand ID

	nite::Point3f		m_HandPosition3D; 	///< Current hand position in real worl coordinates
	nite::Point3f		m_InitPosition3D; 	///< Intial hand position in real worl coordinates

	TBoundingBox		m_BoundingBox; 		///< Bounding box structure for hand "boundaries"
	float 				m_ClickDistance; 	///< Distance from initial position
	int 				m_Type; 			///< Type of the hand
public: 
	/// Constructor 
	/**
		Sets default values to stored private variables defined in this class.  
		\param[in] clickDistance Distance from initial position which will be used to click gesture detection.  
	*/	
						CHand( float clickDistance ); 
	/// Destructor 					
						~CHand( void ){}; 

	/// ID setter  
	/**
		Sets hand ID to a private variable.
		\param[in] id NiTE HandID 
		\see NiTE2 documentation   
	*/	
	void 				setId( nite::HandId id );
	/// ID getter
	/**
		\return Hand ID   
	*/		
	nite::HandId 		getId( void ) const; 
	/// Type setter  
	/**
		Sets hand type to a private variable.
		\param[in] type Hand type (see Common.h)   
	*/		
	void 				setType( int type );
	/// Type getter
	/**
		\return Hand type   
	*/	
	const int 			getType( void ) const;   
	/// Current position getter 
	/**
		\return Hand Current position   
	*/	
	const nite::Point3f getCurrentHandPosition( void ) const; 
 
 	/// Creates new bounding box structure   
	/**
		The function calculates bounding box dimensions from given sideDistance and aspect ration parameters. It also calculates top left point of the 
		bounding box by given relative mouse position. Example which explains mouse relative positon: When mouse is located in 3/4[x,y] of a current screen 
		then hand has to be located in 3/4[x,y] of current bounding box.  
		\param[in] sideDistance Total width of created bounding box 
		\param[aspect] aspect Aspect ratio of screen  
		\param[in] relativeX Relative X position of the mouse     
		\param[in] relativeY Relative Y position of the mouse   
	*/	
	void 				setBoundingBox( float sideDistance, float aspect, float relativeX, float relativeY ); // procenta, kde se nachazi mys  

	/// Hand position setter  
	/**
		Sets hand position to a private variable. If new hand is detected then initial position has to be stored too. 
		\param[in] handPosition3D Hand new position   
		\param[in] init True if position is inital (default = false)  
		\see NiTE2 documentation 
	*/		
	void 				setHandPosition( const nite::Point3f & handPosition3D, bool init = false );

	/// Getter of relative position of the hand in bounding box 
	/**
		Calculates relative hand position in bounding box and sets an output variables to this values. 
		\param[out] relativeX Relative X position of the hand  
		\param[out] relativeY Relative Y position of the hand  
	*/		
	void 				getRelativePosition( float * relativeX, float * relativeY ); 

	/// Click gesture recognizer  
	/**
		Checks distance from initial position. When the distance is greater than click distance defined in configuration file then click gesture is 
		performed. 
		\param[in] choice Defines button to be pressed if click gesture is performed    
		\return True if check gesture was perfomed. 
	*/	
	bool 				checkForClickGesture( int choice ); 

};

#endif 