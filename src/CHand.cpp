//----------------------------------------------------------------------------------------
/**
 * \file       CHand.cpp
 * \author     Lukas Sedlacek (sedlalu2)
 * \date       2015/02/25
 * \brief      Hand representer    
*/
//----------------------------------------------------------------------------------------
#include <iostream> 
#include <cmath> 

#include "CHand.h"
#include "Common.h"

CHand::CHand( float clickDistance ){
	m_ClickDistance = clickDistance; 
} 

void CHand::setId( nite::HandId id ){
	m_Id = id; 	
}
nite::HandId CHand::getId( void ) const{
	return m_Id; 	
}

void CHand::setType( int type ){
	m_Type = type; 
}

const int CHand::getType( void ) const{
	return m_Type; 
}

const nite::Point3f CHand::getCurrentHandPosition( void ) const{
	return m_HandPosition3D; 	
}

void CHand::setBoundingBox( float sideDistance, float aspect, float relativeX, float relativeY ){ // procenta, kde se nachazi mys  

	float width = sideDistance; 
	float height = width / aspect; 

	float x = m_HandPosition3D.x + ( relativeX * width ); // POZOR! OSA X roste doleva u kinectu
	float y = m_HandPosition3D.y + ( relativeY * height ); 

	nite::Point3f boxPosTL( x, y, 0 );
	m_BoundingBox.initializeBox( boxPosTL, width, height ); 

}

void CHand::setHandPosition( const nite::Point3f & handPosition3D, bool init ) { 
	m_HandPosition3D = handPosition3D; 
	
	if ( init ){ // first detection
		m_InitPosition3D = handPosition3D; 		
	}
}


void CHand::getRelativePosition( float * relativeX, float * relativeY ){

	m_BoundingBox.checkHandPosition( m_HandPosition3D ); // may change position 

	nite::Point3f relative; 
	m_BoundingBox.getRelativePositionInBox( m_HandPosition3D, relative );

	*relativeX = relative.x; 
	*relativeY = relative.y; 
}

bool CHand::checkForClickGesture( int gesture ){

	switch( gesture ){
		case LR_CLICK_GESTURE:
			return ( ( m_InitPosition3D.z - m_HandPosition3D.z ) >= m_ClickDistance );
		case WHEEL_CLICK_GESTURE: 
			return ( ( m_HandPosition3D.y - m_InitPosition3D.y ) >= m_ClickDistance ); // OSA Z 
	}
	return true; // non-reachable
}
