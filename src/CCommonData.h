//----------------------------------------------------------------------------------------
/**
 * \file       CCommonData.h
 * \author     Lukas Sedlacek (sedlalu2)
 * \date       2015/02/25
 * \brief      Configuration file loader    
 *
 * Class provides functions to load and process data from a configuration file. This loader requires the configuration files in INI format. 
 * Every configuration file input (section, attribute, value) is saved to multimap where header is a map key and (attribut=value) is a 
 * map value. When the configuration file is missing then default values are used. Also when some values are broken then this values are 
 * set as defaults. 
*/
//----------------------------------------------------------------------------------------
#ifndef _CCOMMONDATA_H_SEDLALU2
#define _CCOMMONDATA_H_SEDLALU2

#include <string> 
#include <map> 

/// Defines attribute=value type 
typedef struct {
	std::string 	m_Attribute;  
	std::string 	m_StringVal; 
} iniData;

/// Defines multimap type for data storage 
typedef std::multimap<std::string, iniData> mmap;  
/// Defines multimap iterator type  
typedef std::multimap<std::string, iniData>::iterator mmapit;

/// Class holds information about data loaded from file
/**
	Class requires path to configuration file. All loaded data are saved in m_Data multimap. Class provides all input validations
	and default sets. Values can be returned as string or as float depends on called method.   
*/
class CCommonData { 

	mmap 				m_Data; ///< Multimap for data storage 

	/// Checks whether line is a comment
	/**
		The function gets a line as a parameter and checks whether line is a comment (if first obtained non-whitespace symbol is a semicolon). 
		If the line is not a comment, then function will create substring of given line by deleting whitespaces at the beginning of the line.   
		\param[in] line Line loaded from configuration file 
		\param[out] line Line without whitespaces at the beginning 
		\return True if line was a comment 
	*/	
	bool				isComment( std::string & line ); 

	/// Checks whether line is a header
	/**
		The function gets a line as a parameter, the it will remove all whitespaces at the beginning and at the end of given line. Then it checks 
		whether line is a header (line is a header if first and last symbol are brackets []). If line is a header, the it is returned as a string, 
		otherwise line without whitespaces is returned.  
		\param[in] line Line loaded from configuration file 
		\param[out] line Line without whitespaces
		\param[out] header Header which was obtained in the line
		\return True if line was a header 
	*/		
	bool				isHeader( std::string & line, std::string & header ); 

	/// Checks whether line is a header 
	/**
		The function will take IP address loaded from configuration file as a parameter and checks if it is a valid IP address. Validity is based on 
		IP address format validation. Address could be "localhost" too. 
		\param[in] address Ip address of SAGE2 websocket server
		\return True if address was valid
 	*/	
	bool 				controlAddress ( const std::string & address ) const; 

	/// Checks values legality 
	/**
		The function will take attribute=value pair as a parameter. Then it will check if value assigned to attribute is valid (for example: not negative value,
		legal string, etc.). Value's validity depends on given attribute. That means configuration file can contain more attributes of same name (in different sections), 
		but all values assigned to this type of attributes will be checked by same validity rule.
		\param[in] attribute Attribute to which is value assigned 
		\param[in] value Checked value  
		\return True if given combination (attibute=value) was valid 
 	*/	
	bool 				checkValueLegal( const std::string & attribute, const std::string & value ) const; 

	/// Line parser
	/**
		Parses line which contains attribute=value pair. The function also checks line validity. Line has to contain equals delimiter, attribute, value and nothing more. 
		\param[in] line Line contained attribute=value pair 
		\param[out] attribute Parsed attribute   
		\param[out] value Parsed value   
		\return True if given line was valid
 	*/		
	bool 				parseLine( std::string & line, std::string & attribute, std::string & value );

	/// Finds value in multimap
	/**	
		Finds value in multimap by given section's header and attribute to which is value assigned. When value is found then iterator to value's position in multimap 
		is returned, when it is not then iterator to multimap's end is returned. 
		\param[in] header Header of section where attribute is contained  
		\param[in] attribute Attribute to which is value assigned    
		\return multimap iterator. 
 	*/	
	mmapit				findValueIterator( const std::string & header, const std::string & attribute ); 

	/// Inserts new value into map 
	/**	
		The function will insert a value into multimap. When value assigned to attribute in specific section with given header already exists in multimap, 
		then value is replaced by new given value. This happens especially when defaults are replaced with values from configuration file.  
		\param[in] header Section's header   
		\param[in] attribute Attribute to which is value assigned    
		\param[in] value value to be inserted in multimap     
 	*/	
	void 				insertIntoMap( const std::string & header, const std::string & attribute, const std::string & value ); 

	/// Finds value in multimap
	/**	
		The function calls findValueIterator. Then checks if value by given header and attribute exists in multimap. Found value is returned as a string. 	
		\param[in] header Section's header   
		\param[in] attribute Attribute to which is value assigned    
		\return Value as string if it is found, when it is not then empty string is returned     
 	*/
	const std::string 	findValue( const std::string & header, const std::string & attribute ); 

public: 
	/// Constructor 
	/**	
		In constructor are set all default values defined in configuration file. 	   
 	*/
						CCommonData( void ); 
	/// Loads data from configuration file  
	/**	
		The function loades lines from configuration files. Lines are processed by private functions of this class (all checks and parses). When values in 
		configuration file are broken or even configuration file is missing then default values are used (no exeption is thrown!).
		\param[in] filepath Path to configuration file.  	  
 	*/						
	void 				loadData( const char * filepath );

	/// Returns value as a string   
	/**	
		Finds value by given header and attribute and returns it as a string. 
		\param[in] header Section's header   
		\param[in] attribute Attribute to which is value assigned  
		\return Value as a string 	  
 	*/			
	const std::string 	getStringValue( const std::string & header, const std::string & attribute ); 

	/// Returns value as a float number   
	/**	
		Finds value by given header and attribute and returns it as a float number. 
		\param[in] header Section's header   
		\param[in] attribute Attribute to which is value assigned  
		\return Value as a float number 	  
 	*/		
	const float 		getNumberValue( const std::string & header, const std::string & attribute ); 

};

#endif 