//----------------------------------------------------------------------------------------
/**
 * \file       CException.h
 * \author     Lukas Sedlacek (sedlalu2)
 * \date       2015/02/25
 * \brief      Main exception class 
 *
 * Class provides processing of a thrown exception. The thrown exception is stored in this class until it is caught and printed out. 
*/
//----------------------------------------------------------------------------------------
#ifndef _CEXEPTION_H_SEDLALU2
#define _CEXEPTION_H_SEDLALU2

#include <string> 

/// Class thrown as exception
/**
	When application fails then this class is thrown. An exception message could be returned by getter defined in this class. Thrown message are defined in 
	Common.h. 
*/
class CException { 

	std::string m_Exception;  ///< Exception message  

public: 
	/// Constructor 
	/**
		Stores thrown message. 
		\param[in] exception Message to be stored in this class.   
	*/	
						CException( const char * exception );
						
	/// Exception message getter  
	/**
		\return Message which was stored in this class (thrown message).   
	*/	
	const std::string 	getException( void ) const; 
};


#endif 