//----------------------------------------------------------------------------------------
/**
 * \file       CTracker.h
 * \author     Lukas Sedlacek (sedlalu2)
 * \date       2015/02/25
 * \brief      Main hand tracker class based on NiTE2 HandTracker class    
 *
 * This is the main hand tracker. Class is based on NiTE2 HandTracker class. The tracker 
 * is used for detecting new hand and processing it's movements. Tracker has two states: Mouse tracking and scrolling state. 
 * 
 * While mouse tracking, tracker obtains hand's position (relatively due to hand's bounding box) and set's mouse to this relative 
 * position. 
 * While scrolling state mouse is not tracked. Tracker could be switched to scrolling state only if two hands are detected. While scrolling 
 * hands distance is calculated. Difference between this calculated distance and last known hands distance is set to CMouseMapper as wheelDelta.  
*/
//----------------------------------------------------------------------------------------
#ifndef _CTRACKER_H_SEDLALU2
#define _CTRACKER_H_SEDLALU2

#include <NiTE.h>
#include <OpenNI.h>
#include <vector> 

#include "CHand.h"
#include "CMouseMapper.h"
#include "CSageNetClient.h"
#include "CCommonData.h"

/// Defines type of pointers to mouse and gesture hand
typedef struct { 
	CHand * m_Mouse; 
	CHand * m_Gesture; 
} THands;

/// Class holds and process information needed for hand tracking. 
/**
	Class creates new NiTE2 HandTracker. This tracker has implemented functions for hand detection and 
	getting hand position out of read depth frames. This detected hands and positions are processed depending on tracker's state (mouse tracking or 
	scrolling). Some values are load from configuration file (section HandTracker). 
	\see NiTE2 documentation
*/
class CTracker { 

	nite::HandTracker			m_HandTracker; 		///< NiTE HandTracker
	nite::HandId 				m_HandId; 			///< Hand ID
 	nite::HandTrackerFrameRef 	m_Frame;			///< Current depth frame loaded from opened device 
 
 	THands						m_HandsList; 		///< Pointers to mouse and gesture hand
 	CMouseMapper				m_MouseMapper; 		///< CMouseMapper instance
 	CCommonData * 				m_CommonData; 		///< Pointer to configuration file loader
 
 	float 						m_BBoxWidth; 		///< Width of bounding box
 	float 						m_ClickDistance; 	///< Click distance for gesture detection 
 	float 						m_HandDistance; 	///< Distance between hands
 	float 						m_Sensitivity; 		///< Minimal user's distance from sensor
 	float 						m_TrackingRange;	///< Tracking range counted from minimal distance
 
	int 						m_State; 			///< State identifier

	/// Checks if hand is in tracking range 
	/**
		Tracking range begins at user's minimal distance from sensor.
		\param[in] position Hand's position in real world coordinates
		\return TRUE if hand is in tracking range, FALSE otherwise
	*/	
	bool			inTrackingRange( const nite::Point3f & position ) const;  
	/// Returns distance between hands
	/**
		The function calculates euclidean distance betwen hands defined in the hands list (between mouse and gesture hand). 
		\return Distance between hands. 
	*/		
	const float		getHandDistance( void ) const;
	/// Updates tracker state
	/**
		Checks wheter start scrolling gesture was detected. If true then status of the tracker is set to scrolling state, otherwise 
		mouse tracking state is set. 
		If tracker is set to scrolling state, then "pointerStartScroll" message is sent to server by CMouseMapper and  
		initial hands distance is calculated. 
		If tracker is set back to mouse tracking state then mouse hand's bounding box has to be updated. 
	*/		
	void 			updateTrackerState( void ); 
	/// Processes new hand
	/**
		If new hand is detected and no other hands were already in hands list then this hand is set as mouse hand and is assigned to the hands list. 
		Then if there is mouse hand already defined in hands list then gesture hand is assigned in hands list. In other cases no hands are stored. 
		While new mouse hand is detected new bounding box has to be created. 
		\param[in] hand Pointer to hand found in actual frame. 
	*/		
	void 			processNewHand( nite::HandData * hand ); 	
	/// Processes hands movements
	/**
		Depending on the types of the hands (mouse or gesture hand) are hands movements processed.
		The mouse hand is set to perform mouse movements. This means that in every frame is actual mouse position (and delta between last known position)
		at screen calculated from actual mouse hand position in its defined bounding box. Mouse hand could also perform click gesture. 
		Gesture hand is set to perform gestures. It can provide two types of gesture: LR click gesture and wheel click gesture (see Common.h)
		\param[in] activeHand Pointer to hand which movement I want to process 
	*/		
	void 			processHandsMovements( CHand * activeHand ); 
	/// Processes click gesture
	/**
		Checks whether hand is in click gesture (see CHand) according to defined gestureType. If true then mouse press event is sent through 
		CMouseMapper. Pressed button is specified in parameter buttonType.  
		\param[in] hand Pointer to hand which click gesture is function checking 
		\param[in] gestureType Type of performed gesture 
		\param[in] buttonType Type of button to be send to SAGE2 server. 
	*/		
	void 			processClickGesture( CHand * hand, int gestureType, int buttonType ); 
	/// Processes scrolling
	/**
		This function is called when tracker is in scrolling state. New distance between the hands has to be calculated. Difference between new 
		and last known distance is called wheel delta. This delta is sent through CMouseMapper to SAGE2 websocket server as JSON message. 
	*/			
	void 			processScrolling( void ); 
	/// Returns active hand
	/**
		Checks whether mouse or gesture hand is active. Hand is active when its id is equal to id of hand which was found in actual frame. 
		\param[in] hand Pointer to the hand found in actual frame. 
		\return Pointer to the active hand or NULL if there are no active hands. 
	*/			
	CHand *			getActiveHand( nite::HandData * hand ) const; 
	/// Counts new bounding box
	/**
		New bounding box is calculated depending on relative mouse position (see CMouseMapper)
	*/
	void 			updateHandBox( void ); 
	/// Frees specifies hand
	/**
		The function frees hand from hand's list specifies by parameter handType (see Common.h). 
		\param[in] handType Type of hand to be freed 
	*/	
	void			freeHandsList( int handType ); 
	/// Frees losted hand
	/**
		If hand is losted then all memory allocated by this hand has to be freed. For freeing memory allocated by any hand is called freeHandsList() with the 
		type of losted hand. Also if hand is lost during gesture performance, then setMouseUp (see CMouseMapper) has to be called.  
		\param[in] lostedHand Pointer to hand which was losted. 
	*/		 			
	void 			freeLostedHand( CHand * lostedHand ); 

public:
	/// Constructor
	/**
		Sets class private variables to default values. 
	*/		
					CTracker( void );
	/// Destructor 
	/**
		Calls freeHandsList method. 
	*/							
					~CTracker( void );
	/// Creates new hand tracker
	/**
		The function creates new hand tracker with specified device. Then checks validity of hand tracker creation. 
		\param[in] device Pointer to opened depth device. 
		\return FALSE if hand tracker creation failed. 
	*/	
	bool			setDevice( openni::Device * device ); 
	/// Initializes tracker and CMouseMapper
	/**
		In this function is tracker initialized. All available tracker constants are loaded from configuration file. Then mouse mapper is 
		initialized too by trasmiting websocket client and configuration file loader pointers to it. At the end is hand tracker set to smooth.  
		\param[in] wsClient Pointer to websocket client.  
		\param[in] common Pointer to configuration file loader.  
	*/		
	void 			initializeHandMapping( CSageNetClient * wsClient, CCommonData * common ); 
	/// Starts detecting hands
	/**
		The function starts hands detection and it also starts SAGE2 pointer by sending start message to it through CMouseMapper. 
	*/	
	void 			start( void ); 
	/// Stops detecting hands
	/**
		The function stops hands detection and it also stops SAGE2 pointer by sending stop message to it through CMouseMapper. 
	*/		
	void 			stop( void );
	/// Reads frame
	/**
		The function reads frames from opened OpenNI2 device (\see OpenNI2 documentation) and checks its validity. 
		\return FALSE if readed frame was not valid
	*/	
	bool		 	readFrame( void ); 
	/// Releases frame
	/**
		The function releases memory allocated by one single frame.  
	*/		
	void			releaseFrame( void ); 
	/// Detects hands in a frame
	/**
		Checks whether gesture (hand raise) was performed. If true then tracking range control is provided. After all checks hands is going to 
		be successfully tracked.
	*/	
	void 			handDetection( void );
	/// Maps hands gestures to mouse events
	/**
		If hand is new, then some initial procedures are called (by function processNewHand()). Otherwise hand could be lost, so free function is called. 
		In the other cases function only checks in which state is hand tracker standing. Depending on that is called scrolling or mouse movement function.   
	*/		
	void			handMapping( void );  
	/// Destroys created hand tracker
	/**
		Function destroys NiTE HandTracker. 
		\see NiTE2 documentation
	*/	
	void 			closeTracker( void ); 
};

#endif 
