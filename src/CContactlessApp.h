//----------------------------------------------------------------------------------------
/**
 * \file       CContactlessApp.h
 * \author     Lukas Sedlacek (sedlalu2)
 * \date       2015/02/25
 * \brief      Main contacless app class  
 *
 * Class provides basic functions for SAGE2 contacless handling. Main purpose of this class is to initialize (or load) a configuration file, network connection,
 * device and hand tracker. Then it provides a main application loop which could be break by keyboard press (q or Q).
*/
//----------------------------------------------------------------------------------------
#ifndef _CCONTACTLESSAPP_H_SEDLALU2
#define _CCONTACTLESSAPP_H_SEDLALU2 

#include <OpenNI.h> 
#include <NiTE.h> 
#include <string> 
#include "CTracker.h"
#include "CSageNetClient.h"
#include "CCommonData.h"

/// Class holds basic information about application
/**
	Basic class of the application for intialization and main looping. Class holds information about device, hand tracker and it also holds some 
	pointers on important classes like websocket client or configuration file loader. 
*/
class CContactlessApp { 

	openni::Device 		m_Device;			///< Class representation of current device (OpenNI2)
	const char * 		m_DeviceURI; 		///< String which represents device which is openned 
	CTracker			m_HandTracker; 		///< Class representation of created hand tracker (NiTE2)
	CSageNetClient * 	m_WSClient; 		///< Pointer to websocket client 
	CCommonData * 		m_CommonData; 		///< Pointer to configuration file loader 

	/// Initializes new configuration loader class 
	/**
		Creates new configuration loader class and loads data from config file. Configuration file path is assigned as parametr.  
		\param[in] configPath Path to configuration file 
	*/	
	void 	initializeConfig( const char * configPath ); 

	/// Initializes new websocket client  
	/**
		Creates new websocket client by given address and port as parameters. Also tries to connect to websocket server (SAGE2 server)  
		\param[in] address Ip address of websocket server 
		\param[in] address Port given as string 
		\throw CException when client cannot not be connected to server.  
	*/	
	void 	initializeNetwork( const std::string & address, const std::string & port ); 
	
	/// Initializes OpenNI2 API and device  
	/**	
		OpenNI2 API is initialized in this function. Then device is opened using DeviceURI string. By default is application set to 
		accept any depth device connected to running computer. 
		\throw CException when initialization or opening device failed. 
		\see OpenNI2 documentation   
	*/
	void 	initializeDevice( void );

	/// Initializes NiTE2 API and creates HandTracker class  
	/**	
		NiTE2 API is initialized in this function, then new hand tracker is created by assignment of openned device. The tracker is also intialized 
		by calling initializeHandMapping function (see CTracker). 
		\throw CException when initialization or creating hand tracker failed. 
		\see NiTE2 documentation   
	*/	
	void 	initializeHandTracker( void );	

public: 
	/// Constructor 
	/**
		Sets default values to this class's private variables.  
	*/	
			CContactlessApp( void ); 
	/// Destructor 
	/**
		Calls finalizeApp function in case user forgot to call it. 
	*/ 
			~CContactlessApp( void ); 

	/// Public "package" for initializations
	/**
		Calls all private methods for initialization.  
		\param[in] configPath Path to configuration file
		\throw CException when any of initializations fails. 
	*/	
	void 	initializeApp( const char * configPath ); 	

	/// Application's main loop
	/**
		Starts HandTracker (see CTracker class) and reads frame in the main loop. Every frame is processed by CTracker methods. 
		\see NiTE::HandTracker on which is CTracker based on. 
	*/		
	void 	mainLoop( void ); 

	/// Finalize application and clears all allocated memory
	/**
		Function calls destroy functions on OpenNI2 and NiTE2 objects. Then also frees memory allocated by websocket client and configuration file loader.
		\see NiTE2 and OpenNI2 documentation
	*/		
	void 	finalizeApp( void ); 
};

#endif 
