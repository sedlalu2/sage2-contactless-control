//----------------------------------------------------------------------------------------
/**
 * \file       TBoundingBox.h
 * \author     Lukas Sedlacek (sedlalu2)
 * \date       2015/02/25
 * \brief      Structure which represents hand's bounding box  
 *
 * When new hand is detected and it is identified by the program as a MOUSE hand, then new bounding box is created around this hand. 
 * Bounding box has same aspect ratio as screen defined in configuration file, so mouse movement is conveyed very smooth. For Point3f documentation 
 * \see NiTE2 documentation
*/
//----------------------------------------------------------------------------------------
#ifndef _TBOUNDING_BOX_H_SEDLALU2
#define _TBOUNDING_BOX_H_SEDLALU2

#include "NiTE.h"

/// Struct holds information of bounding box
/**
	Struct provides basic functions for work with bounding box, like checking hand position in bounding box, creating new bounding box or returning relative 
	hand position in bounding box. 
*/
struct TBoundingBox { 

	nite::Point3f 	m_TLPosition2D; ///< Position of top left bounding box point 		
	nite::Point3f	m_BRPosition2D; ///< Position of bottom right bounding box point

	float 			m_BoxHeight;	///< Bounding box height in real world units 
	float 			m_BoxWidth; 	///< Bounding box width in real world units

	/// Creates new bounding box  
	/**
		By given top left point and bounding box dimensions is calculated bottom right point which is stored in this structure. 

		\param[in] position2D Position of top left point given in 2D coordinates (uses Point3f with z equals to zero)
		\param[in] boxWidth Width of created bounding box given in real world units. 
		\param[in] boxHeight Height of created bounding box given in real world units.
	*/	
	void 			initializeBox( const nite::Point3f position2D, float boxWidth, float boxHeight ); 

	/// Checks hand position in bounding box  
	/**
		Bounding box is intialized - this position is fixed for whole hand life. If hand is out of bounding box, then hand's coordinates has to be 
		changed to bounding box border coordinates.
		\param[in] handPosition3D Position of hand in real world. It is given in 3D coordinates but only XY coordinates are used to check position. 
		\param[out] handPosition3D Changed hand position coordinates. 
	*/		
	void 			checkHandPosition( nite::Point3f & handPosition3D ); 

	/// Returns a relative position in bounding box   
	/**
		Function returns a position of the hand in bounding box in relative coordinates (this means that returned coordinates are 0-1 numbers - percentages)
		\param[in] handPosition3D Position of hand in real world.
		\param[out] relative2D Relative hand position in bounding box.  
	*/		
	void 			getRelativePositionInBox( const nite::Point3f & handPosition3D, nite::Point3f & relative2D );	
};

#endif