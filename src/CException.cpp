//----------------------------------------------------------------------------------------
/**
 * \file       CException.cpp
 * \author     Lukas Sedlacek (sedlalu2)
 * \date       2015/02/25
 * \brief      Main exception class 
*/
//----------------------------------------------------------------------------------------
#include "CException.h"

CException::CException( const char * exception ){
	m_Exception = exception; 	
}
	
const std::string CException::getException( void ) const{
	return "(ERROR) " + m_Exception; 
} 