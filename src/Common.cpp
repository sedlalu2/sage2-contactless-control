//----------------------------------------------------------------------------------------
/**
 * \file       Common.cpp
 * \author     Lukas Sedlacek (sedlalu2)
 * \date       2015/02/25
 * \brief      Common definitions   
*/
//----------------------------------------------------------------------------------------
#include <unistd.h>
#include <stdlib.h>
#include <termios.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <sstream> 
#include <iostream> 

#include "Common.h"
#include "CException.h"
using namespace std; 


void printToConsole( const std::string & message ){

    string output = MESSAGE_PREFIX + message; 
    cout << output << endl;
} 

const char * handleConsoleInput( int argc, char const **argv ){

    if ( argc != 3 || ( ( argc == 3 ) && ( strcmp( argv[1], "-f" ) != 0 ) ) ) 
        throw CException( MESSAGE_APP_USAGE ); 

    return argv[2]; 
}

const string enumToStrButton( int choice ){

    string button = ""; 
    switch( choice ){
        case DUMMY_BUTTON: 
            break; 
        case LEFT_BUTTON: 
            button = "left"; 
            break;
        case MIDDLE_BUTTON: 
            button = "middle"; 
            break;
        case RIGHT_BUTTON: 
            button = "right"; 
            break;
    }
    return button; 
}

bool keyboardHit() {

    struct termios oldt, newt;
    int ch;
    int oldf;

    // don't echo and don't wait for ENTER
    tcgetattr( STDIN_FILENO, &oldt );
    newt = oldt;
    newt.c_lflag &= ~( ICANON | ECHO );
    tcsetattr(STDIN_FILENO, TCSANOW, &newt);
    oldf = fcntl(STDIN_FILENO, F_GETFL, 0);
    
    // make it non-blocking (so we can check without waiting)
    if ( 0 != fcntl( STDIN_FILENO, F_SETFL, oldf | O_NONBLOCK ) ) {
        return 0;
    }

    ch = getchar();

    tcsetattr( STDIN_FILENO, TCSANOW, &oldt );
    if ( 0 != fcntl( STDIN_FILENO, F_SETFL, oldf ) ) {
        return 0;
    }

    if( ch != EOF ) {
        if ( ch != 113 && ch != 81 ) return 0; // q or Q ASCII
        ungetc( ch, stdin );
        return 1;
    }

    return 0;
}

float toNumber ( const string & str ) {
    stringstream ss( str );
    float result;
    return ss >> result ? result : -1;
}

// if number is negative, then -1 is returned! 
float isNumber ( const string & str, bool integer ) {
    
    int commas = 0;
        for( unsigned int i = 0; i < str . length(); i++ ){
        if ( integer == true ){
            if ( !isdigit( str[i] ) ) return -1;            
        } else { 
            if ( str[i] == '.' ) commas++;
            if ( (!isdigit( str[i] ) && str[i] != '.') || commas > 1 ){
                return -1;
            }
        }
    }
    return toNumber( str );
}

const string toString ( const int & number ){
    ostringstream ss;
    ss << number;
    return ss.str();
}
