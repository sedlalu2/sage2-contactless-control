//----------------------------------------------------------------------------------------
/**
 * \file       TBoundingBox.cpp
 * \author     Lukas Sedlacek (sedlalu2)
 * \date       2015/02/25
 * \brief      Structure which represents virtual bounding box  
*/
//----------------------------------------------------------------------------------------
#include <cmath> 
#include "TBoundingBox.h"

void TBoundingBox::initializeBox( const nite::Point3f position2D, float boxWidth, float boxHeight ){

	m_TLPosition2D	= position2D; 
	m_BoxWidth		= boxWidth; 
	m_BoxHeight		= boxHeight; 

	m_BRPosition2D.set( m_TLPosition2D.x - m_BoxWidth, 
						m_TLPosition2D.y - m_BoxHeight, 
						m_TLPosition2D.z );
}

void TBoundingBox::checkHandPosition( nite::Point3f & handPosition3D ){ 

	// top left 
	if ( handPosition3D.x >= m_TLPosition2D.x ) handPosition3D.x = m_TLPosition2D.x;
	if ( handPosition3D.y >= m_TLPosition2D.y ) handPosition3D.y = m_TLPosition2D.y; 
	
	// bottom right
	if ( handPosition3D.x <= m_BRPosition2D.x ) handPosition3D.x = m_BRPosition2D.x; 
	if ( handPosition3D.y <= m_BRPosition2D.y ) handPosition3D.y = m_BRPosition2D.y;
}

void TBoundingBox::getRelativePositionInBox( const nite::Point3f & handPosition3D, nite::Point3f & relative2D ){

	float x = std::abs( ( m_TLPosition2D.x - handPosition3D.x )/m_BoxWidth );
	float y = std::abs( ( m_TLPosition2D.y - handPosition3D.y )/m_BoxHeight );

	relative2D.set( x, y, 0 ); 
}
