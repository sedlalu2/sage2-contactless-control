//----------------------------------------------------------------------------------------
/**
 * \file       CContactlessApp.cpp
 * \author     Lukas Sedlacek (sedlalu2)
 * \date       2015/02/25
 * \brief      Main contacless app class  
*/
//----------------------------------------------------------------------------------------
#include <string.h>

#include "CContactlessApp.h"
#include "CException.h"
#include "Common.h"
#include "CHand.h"
#include "CMouseMapper.h"

using namespace std; 

void CContactlessApp::initializeConfig( const char * configPath ){
	m_CommonData = new CCommonData; 
	m_CommonData->loadData( configPath ); 
}

void CContactlessApp::initializeNetwork( const std::string & address, const std::string & port ){

	m_WSClient = new CSageNetClient( address, port );

	if ( !m_WSClient->connectToServer() ) { 
		throw CException( MESSAGE_EXC_CONNECTION );
	} 

	m_WSClient->initialize(); 
} 

void CContactlessApp::initializeDevice( void ){ 

	openni::Status stat = openni::STATUS_OK;

	stat = openni::OpenNI::initialize(); 
	if ( stat != openni::STATUS_OK ){
		throw CException( openni::OpenNI::getExtendedError() ); 
	}

	stat = m_Device.open( m_DeviceURI ); 
	if ( stat != openni::STATUS_OK ){
		throw CException( openni::OpenNI::getExtendedError() ); 	
	}
}

void CContactlessApp::initializeHandTracker(){ 

	nite::Status stat = nite::STATUS_OK; 

	stat = nite::NiTE::initialize(); 
	if ( stat != nite::STATUS_OK ){
		throw CException( MESSAGE_EXC_HT_INIT ); 
	}

	if ( !m_HandTracker.setDevice( &m_Device ) ) {
		throw CException( MESSAGE_EXC_HT_CREATE );
	}	

	m_HandTracker.initializeHandMapping( m_WSClient, m_CommonData ); // set smoothness, resolution... 
}

CContactlessApp::CContactlessApp() { 

	m_DeviceURI = openni::ANY_DEVICE;

	m_WSClient = NULL; 
	m_CommonData = NULL; 
}

CContactlessApp::~CContactlessApp( void ) {
	finalizeApp();
} 

void CContactlessApp::initializeApp( const char * configPath ){ 

	this->initializeConfig( configPath ); 

	string address 	= m_CommonData->getStringValue( "Network", "Address" ); 
	string port 	= m_CommonData->getStringValue( "Network", "Port" );

	this->initializeNetwork( address, port ); 	
	this->initializeDevice(); 
	this->initializeHandTracker(); 	
}

void CContactlessApp::mainLoop( void ){ 

	m_HandTracker.start();

	printToConsole( MESSAGE_APP_START );

	while( !keyboardHit() ){

		// read frame and process it 
		if ( !m_HandTracker.readFrame() ) continue; 

		m_HandTracker.handDetection(); 
		m_HandTracker.handMapping();

		m_HandTracker.releaseFrame(); 
	}

	printToConsole( MESSAGE_APP_STOP );
	m_HandTracker.stop(); 
}

void CContactlessApp::finalizeApp() {	

	if ( m_WSClient != NULL ){
		m_WSClient->closeNetwork(); 
		delete m_WSClient; 
		m_WSClient = NULL; 
	}

	if ( m_CommonData != NULL ){
		delete m_CommonData; 
		m_CommonData = NULL; 
	}

	m_HandTracker.closeTracker(); 
	nite::NiTE::shutdown();  		
	
	m_Device.close(); 
	openni::OpenNI::shutdown(); 	
}
