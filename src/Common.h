//----------------------------------------------------------------------------------------
/**
 * \file       Common.h
 * \author     Lukas Sedlacek (sedlalu2)
 * \date       2015/02/25
 * \brief      Common definitions   
 *
 * File contains all definition which are common for whole application (like list of message, exceptions and then enumerations and 
 * some common functions). 
*/
//----------------------------------------------------------------------------------------
#ifndef _FUNCTIONS_H_SEDLALU2
#define _FUNCTIONS_H_SEDLALU2

#include <string> 

#define MESSAGE_PREFIX			"CONTACTLESS SAGE APP: "

#define MESSAGE_APP_START 		"Detection started. Raise your hand a bit! TO QUIT PRESS Q OR q!"
#define MESSAGE_APP_STOP		"Finalizing application!"
#define MESSAGE_APP_USAGE		"Usage of program: ./program_name -f path/to/config"

#define MESSAGE_CONFIG_LOAD 	"(WARNING) File failed to load configuration file. Using default values" 
#define MESSAGE_CONFIG_BAD		"(WARNING) Some values will be default, because configuration file is broken."
#define MESSAGE_CONFIG_VALUES	"(WARNING) Some values will be default, because there are invalid values in configuration file."

#define MESSAGE_EXC_CONNECTION 	"Cannot connect to server."
#define MESSAGE_EXC_HT_INIT 	"Hand tracker initialization failed."
#define MESSAGE_EXC_HT_CREATE 	"Creating hand tracker failed."
#define MESSAGE_EXC_FRAME_SKIP 	"Frame was broken. I skipped it."

#define MESSAGE_TRACKER_OUT		"Some hands are out of tracking range!" 

/// Defines hand types 
enum handType {  
	HAND_MOUSE,		///< Hand used for hand tracking  
	HAND_GESTURE,	///< Hand used preferably for gestures 
	HANDS_BOTH		///< Both hands indicator 
};

/// Defines tracker states 
enum trackerStates { 
	SCROLLING_STATE, 		///< Tracker scrolling state. Mouse is not tracked.
	MOUSE_TRACKING_STATE	///< Tracker mouse tracking state.
};

/// Defines click gestures 
enum gestures {
	LR_CLICK_GESTURE, 		///< Click made by left or right button 
	WHEEL_CLICK_GESTURE		///< Click made by middle button 
};

/// Defines mouse buttons 
enum buttons{ 
	DUMMY_BUTTON, 			///< This has to be defined when programmer wants to use X11 library for mouse events. 
	LEFT_BUTTON, 			///< Left button indicator 
	MIDDLE_BUTTON, 			///< Middle button indicator
	RIGHT_BUTTON,			///< Right button indicator
	BUTTONS_NUMBER			///< Number of buttons 
};

/**
	Handles program start. It checks whether program is started with configuration file as a parametr. 
	
	Usage of program: ./program_name -f path/to/config

	\param[in] agrc Count of console inputs  
	\param[in] argv Console inputs in array 
*/
const char * handleConsoleInput( int argc, char const **argv ); 

/**
	Prints given message to console. 
	\param[in] message Message which will be printed out.   
*/
void printToConsole( const std::string & message ); 

/**
	Converts buttons enum type to string values. 
	\param[in] choice Button which will be converted to string value 
	\return Enum value as a string  
*/
const std::string enumToStrButton( int choice );

/**
	Checks if keyboard was hit. Then it performs input validations (is or isn't Q or q key).
	
	The function was originally written in NiTE2 samples. I have made only some small improvements. 

	\see NiTE2 samples 
	\return True if Q or q was hit.   
*/
bool keyboardHit();

/**
	Converts given string to a float value (all values has to be positive). When function fails then -1 is returned. 
	\param[in] str String which will be converted 
	\return Number value given string.    
*/
float toNumber ( const std::string & str );

/**
	Checks whether string is a number. When string is the number then toNumber is called on it, otherwise -1 is returned.
	The function can process float and integer values. 

	\param[in] str String which will be checked
	\param[in] integer Boolean value if given string is integer or not 
	\return String value converted to number value   
*/
float isNumber ( const std::string & str, bool integer = false ); 

/**
	Converts any given number to a string. 
	\param[in] number Number which will be converted to a string.  
	\return Number value as a string. 
*/
const std::string toString( const int & number );

#endif 