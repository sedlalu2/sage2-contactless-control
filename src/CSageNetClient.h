//----------------------------------------------------------------------------------------
/**
 * \file       CSageNetClient.h
 * \author     Lukas Sedlacek (sedlalu2)
 * \date       2015/02/25
 * \brief      Websocket client   
 *
 * Class conveys websocket connection to webserver (on given IP address and port). SAGE2 websocket server expects 
 * messages in JSON strict format. This class prepares those messages which are instantly sent to SAGE2 server.  
*/
//----------------------------------------------------------------------------------------
#ifndef _CSAGENETCLIENT_H_SEDLALU2
#define _CSAGENETCLIENT_H_SEDLALU2

#include <string> 
#include "limits.h"
#include "easywsclient.hpp"

/// Class holds information about websocket connection.
/**
	Class provides functions for initialize new SAGE client, server connection and message sending. There are no functions for message receiving 
	needed. Sending function transforms messages to JSON strict format required by SAGE2 websocket server. Class is based on easywsclient. 
	
	\see Reference https://github.com/dhbaird/easywsclient 
*/	
class CSageNetClient { 

	std::string 						m_URL; 			///< Websocket server speficied URL
	easywsclient::WebSocket::pointer	m_WebSocket;	///< Websocket pointer 

	/// Method sends message to websocket server 
	/**
		\param[in] message Message which will be send to server  
	*/	
	void 		sendMsg( const std::string & message ); 
public:
	/// Constructor
	/**
		Constructor requires server hostname (or IP address) and port. It prepares URL which will be used for connection estabilization 
		and sets websocket pointer to NULL. 
		\param[in] hostname IP address or hostname of the server 
		\param[in] port Port given as string.  
	*/		
				CSageNetClient( const std::string & hostname, const std::string & port );
	
	/// Destructor
	/**
		Calls closeNetwork function. 
	*/						
				~CSageNetClient( void );

	/// Connection estabilization
	/**
		The function creates new websocket which represents connection between client and server and estabilished connection to the server. 
		\return True if everything went alright. 
	*/	
	bool 		connectToServer( void ); 

	/// Registrates new client
	/**
		SAGE websocket server is waiting for new clients to be connected. Every client has to send "addClient" message to registrate 
		new websocket client to webserver. The function prepares this type of message and sends it to server. 

		\see https://bitbucket.org/sage2/sage2/wiki/Home for more information about websockets and messages (better way is to download source codes)
	*/		
	void 		initialize( void ); 

	/// Closes network
	/**
		The function checks if client is still connected to the server and eventually closes connection to the server and clears memory allocated 
		by websocket pointer.  
	*/		
	void 		closeNetwork( void ); 

	/// Prepares message and sends it to the server. 
	/**
		The function could prepare three different types of messages which provides pointer events. This messages are send to the server. The function is called 
		on every message, where data type are strings. 

		Predominantly is function used for: stop/start sage pointer, scrolling and buttons presses and releases. 
		
		\param[in] name Name of command which will be send.
		\param[in] data1 Content of string data value 
		\param[in] data2 Content of string data value 
	*/	
	void 		sendMessage( const std::string & name, const std::string & data1 = "", const std::string & data2 = "" ); 

	/// Prepares message and sends it to server. 
	/**
		The function could prepare two different types of messages which provides pointer events. This messages are send to the server. The function is called 
		on every message, where data type are floats. 

		Predominantly is function used for: sending mouse movements deltas and wheel deltas to server.  
		
		\param[in] name Name of command which will be send.
		\param[in] data1 Content of float data value 
		\param[in] data2 Content of float data value 
	*/	
	void 		sendMessage( const std::string & name, const int & data1, const int & data2 = INT_MAX );
};


#endif