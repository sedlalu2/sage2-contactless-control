//----------------------------------------------------------------------------------------
/**
 * \file       CSageNetClient.cpp
 * \author     Lukas Sedlacek (sedlalu2)
 * \date       2015/02/25
 * \brief      Websocket client class   
*/
//----------------------------------------------------------------------------------------
#include <string> 
#include <cstring>
#include <sstream> 
#include <iostream> 
#include "CSageNetClient.h"
#include "Common.h"

using namespace std; 

/******************
	PRIVATE 
******************/

void  CSageNetClient::sendMsg( const string & message ){ 
    m_WebSocket->send( message );
    m_WebSocket->poll();
}

/******************
	PUBLIC
******************/
CSageNetClient::CSageNetClient( const string & hostname, const string & port ){ 

	m_URL = "ws://" + hostname + ":" + port; 
	m_WebSocket = NULL; 
}

CSageNetClient::~CSageNetClient( void ){
	this->closeNetwork(); 
}

bool CSageNetClient::connectToServer( void ) { 

	m_WebSocket = easywsclient::WebSocket::from_url( m_URL );
    if ( m_WebSocket == NULL ){
    	return false;     	
    }
    return true; 
}

//initialize new Websocket client 
void  CSageNetClient::initialize( void ){

	string name = "addClient";
	string strTrue = "true";
	string strFalse = "false";

	string message ="{\"f\":\"" + name + "\"" + 
					",\"d\":{\"clientType\":\"sagePointer\"" + 
					",\"sendsPointerData\":" 				+ strTrue + 
					",\"sendsMediaStreamFrames\":" 			+ strTrue + 
					",\"requestsServerFiles\":" 			+ strFalse + 
					",\"sendsWebContentToLoad\":" 			+ strTrue + 
					",\"launchesWebBrowser\":" 				+ strFalse + 
					",\"sendsVideoSynchonization\":" 		+ strFalse + 
					",\"sharesContentWithRemoteServer\":" 	+ strFalse + 
					",\"receivesDisplayConfiguration\":" 	+ strTrue + 
					",\"receivesClockTime\":" 				+ strFalse + 
					",\"requiresFullApps\":" 				+ strFalse + 
					",\"requiresAppPositionSizeTypeOnly\":"	+ strFalse +  
					",\"receivesMediaStreamFrames\":" 		+ strFalse + 
					",\"receivesWindowModification\":" 		+ strFalse + 
					",\"receivesPointerData\":" 			+ strFalse + 
					",\"receivesInputEvents\":" 			+ strFalse + 
					",\"receivesRemoteServerInfo\":" 		+ strFalse + 
					"}}";	
	this->sendMsg( message );
}

void CSageNetClient::closeNetwork( void ){ 

	if ( m_WebSocket != NULL ){
		if ( m_WebSocket->getReadyState() != easywsclient::WebSocket::CLOSED ){
			m_WebSocket->close(); 
		}
		delete m_WebSocket; 
		m_WebSocket = NULL; 
	}
}

void CSageNetClient::sendMessage( const string & name, const string & data1, const string & data2 ) { 

	string message; 

	if ( data1.empty() && data2.empty() ){ // pouze name 
		message = "{\"f\":\"" + name + "\"}";
	} else if ( !data1.empty() && data2.empty() ){ // name + button 
		message = "{\"f\":\"" + name + "\",\"d\":{\"button\":\"" + data1 + "\"}}"; 
	} else { // name + label + color 
		message = "{\"f\":\"" + name + "\",\"d\":{\"label\":\"" + data1 + "\",\"color\":\"" + data2 + "\"}}";
	}

	this->sendMsg( message );  
}

void CSageNetClient::sendMessage( const string & name, const int & data1, const int & data2 ) {

	string message;

	if ( data2 == INT_MAX ){ // scrolling 
		message = "{\"f\":\"" + name + "\",\"d\":{\"wheelDelta\":" + toString( data1 ) + "}}"; 
	} else { // moving mouse 
		message = "{\"f\":\"" + name + "\",\"d\":{\"dx\":" + toString( data1 ) + ",\"dy\":" + toString( data2 ) + "}}";
	}

	this->sendMsg( message );  
} 

