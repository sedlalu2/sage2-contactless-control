//----------------------------------------------------------------------------------------
/**
 * \file       CMouseMapper.h
 * \author     Lukas Sedlacek (sedlalu2)
 * \date       2015/02/25
 * \brief      Mouse representer    
 *
 * Class provides all functions for mouse movement, buttons clicking and so on. All those events 
 * are send to SAGE2 websocket server.
*/
//----------------------------------------------------------------------------------------
#ifndef _CMOUSEMAPPER_H_SEDLALU2
#define _CMOUSEMAPPER_H_SEDLALU2

#include "CSageNetClient.h"
#include "CCommonData.h"
#include <string> 

/// Class holds all mouse information 
/**
	This class conveys all mouse events captures. The events could be - mouse movement, mouse clicking (left, middle, right button) and scrolling.
	Class has pointer to websocket client which keeps connection with SAGE2 server. All mouse events are sent to SAGE2 server as JSON messages (= all mouse 
	events are mapped to SAGE2 pointer events).	Class also holds information about current SAGE2 screen (screen dimension) 
*/
class CMouseMapper{ 

	int 				m_X; 				///< Mouse X screen coordinate
	int 				m_Y; 				///< Mouse Y screen coordinate
 
 	unsigned int 		m_Width; 			///< Width of screen (in pixels)
 	unsigned int 		m_Height;			///< Height of screen (in pixels)
 
 	CSageNetClient * 	m_WSClient;			///< Pointer to websocket client 
 	bool *				m_PressedButtons;	///< Array represents pressed buttons
 	float 				m_ScrollingSpeed; 	///< Scrolling speed
 
 	std::string 		m_PointerLabel; 	///< Pointer label (displayed on SAGE2)	
	std::string 		m_PointerColor; 	///< Pointer color 
	
public:
	/// Constructor
	/**
		There are no parameters required. Constructor sets all private variables to default values. The array of pressed buttons is initialized (all values 
		are false, because no button was "pressed" yet).
	*/			
			CMouseMapper( void );
	/// Destructor
	/**
		Sends mouse up event to all already pressed buttons and deletes pointer to pressed buttons array.
	*/				
			~CMouseMapper( void ); 

	/// Intializes new mouse mapper
	/**
		The function assigns websocket client pointer to this class. Then all needed data are loaded from configuration file whose pointer is passed as an
		agurment.  
		\param[in] wsClient Pointer to websocket client
		\param[in] common Pointer to common data loader
	*/	
	void 	initialize( CSageNetClient * wsClient, CCommonData * common ); 
	/// Starts SAGE pointer
	/**
		Sends "startSagePointer" message to SAGE2. 
	*/		
	void 	startSagePointer( void );
	/// Starts SAGE pointer scrolling state
	/**
		Sends "pointerScrollStart" message to SAGE2. 
	*/			
	void 	startScrolling( void ); 
	/// Stops SAGE pointer
	/**
		Sends "stopSagePointer" message to SAGE2. 
	*/		
	void 	stopSagePointer( void );  
	/// Returns current mouse position
	/**
		The mouse position is returned depending on current screen. It is returned as relative expression of the position (percentage) due to screen resolution 
		defined in configuration file. 
		\param[out] x Relative mouse position on X axes. 
		\param[out] y Relative mouse position on Y axes. 
	*/		
	void 	getCurrentMousePositionRelative( float * x, float * y );
	/// Returns screen aspect ratio
	/**
		The function returns screen aspect ratio as width/height resolution. 
		\return Screen aspect ratio 
	*/		
	float 	getScreenAspectRatio( void ); 
	/// Sets mouse position by given relatives 
	/**
		The function sets mouse position to coordinates which are calculated by screen dimension and given relative position. Relative position is 
		mostly obtained from CHand getRelativePosition. The function also calculates difference between current and last known mouse position and this 
		deltas are sent to SAGE2. 
		\param[in] x Relative mouse position on X axes. 
		\param[in] y Relative mouse position on Y axes. 
	*/	
	void 	setMouseRelative( float x, float y ); 
	///  Processing of mouse press down event
	/**
		When click gesture is detected, then this function is called (see CHand). It sets TRUE value to pressed buttons array depending on button choice (left, right
		middle). This function sends mouse down event to SAGE2 only once (in case that click gesture is detected more than one time in a row). 
		\param[in] choice Specifies which button should pressed down
		\param[in] fakeClick True if click is only fake (click gesture was not actually performed). 
	*/		
	void 	setMouseDown( int choice, bool fakeClick = false );
	///  Processing of mouse press up event
	/**
		When click gesture is no longer detected, then this fuction is called (see CHand). Sets FALSE to to pressed buttons array depending on button choice (left, right
		middle). 
		\param[in] choice Specifies which button should pressed up
	*/		
	void 	setMouseUp( int choice ); 
	///  Sets scrolling delta
	/**
		The function calculates wheel delta by given wheelDelta parameter and scrolling speed. This value is send to SAGE2. SAGE2 has to be set to scrolling state
		by calling startScrolling() before values are send. 
		\param[in] wheelDelta Detected wheel delta
	*/	
	void 	setScrollingDelta( int wheelDelta ); 

	///  Checks pressed buttons
	/**
		\param[in] choice Button which will be checked on
		\return TRUE if given button is pressed
	*/	
	bool 	isPressed( int choice ); 

};

#endif 