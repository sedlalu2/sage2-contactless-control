#include <OpenNI.h> 
#include <string> 
#include <iostream> 

#include "CContactlessApp.h"
#include "CException.h"
#include "CMouseMapper.h"
#include "CCommonData.h"
#include "Common.h"

using namespace std; 

int main( int argc, char const *argv[] ) {

	CContactlessApp application;
	
	try { 
		const char * configPath = handleConsoleInput( argc, argv );
		application.initializeApp( configPath );
		application.mainLoop(); 
	} catch( CException & exc ){
		printToConsole( exc.getException() ); 
		return 1;
	}
	
	application.finalizeApp(); 
	return 0;
}