//----------------------------------------------------------------------------------------
/**
 * \file       CMouseMapper.cpp
 * \author     Lukas Sedlacek (sedlalu2)
 * \date       2015/02/25
 * \brief      Mouse representer    
*/
//----------------------------------------------------------------------------------------
#include "CMouseMapper.h"
#include "CException.h"
#include "Common.h"
#include <cmath> 

using namespace std; 

CMouseMapper::CMouseMapper( void ){
	m_X = 0; 
	m_Y = 0; 
	m_Width = 0;
	m_Height = 0;
	m_WSClient = NULL; 

	m_PressedButtons = new bool[BUTTONS_NUMBER];
	for ( int i = 0; i < BUTTONS_NUMBER; i++ ){
		m_PressedButtons[i] = false; 
	}
}

CMouseMapper::~CMouseMapper( void ){

	for ( int i = 0; i < BUTTONS_NUMBER; i++ ){
		if ( m_PressedButtons[i] == true ){
			this->setMouseUp( i ); 
		}		
	}

	delete[] m_PressedButtons; 	
}

void CMouseMapper::initialize( CSageNetClient * wsClient, CCommonData * common ){
	m_WSClient = wsClient; 

	m_Width	= ( unsigned int )common->getNumberValue( "Screen", "Width" );  
	m_Height = ( unsigned int )common->getNumberValue( "Screen", "Height" );  
	m_ScrollingSpeed = common->getNumberValue( "Screen", "ScrollingSpeed" );  

	m_PointerLabel = common->getStringValue( "Pointer", "Label" ); 
	m_PointerColor = common->getStringValue( "Pointer", "Color" ); 
} 

void CMouseMapper::startSagePointer( void ){
	m_WSClient->sendMessage( "startSagePointer", m_PointerLabel, m_PointerColor ); 
}

void CMouseMapper::startScrolling( void ){ 
	m_WSClient->sendMessage( "pointerScrollStart" ); 
}

void CMouseMapper::stopSagePointer( void ){
	m_WSClient->sendMessage( "stopSagePointer" ); 
}  

void CMouseMapper::getCurrentMousePositionRelative( float * x, float * y ){ // procenta

	*x = (float)m_X / m_Width;
	*y = (float)m_Y / m_Height; 
}

float CMouseMapper::getScreenAspectRatio( void ){
	return (float)m_Width/m_Height; 	
}

void CMouseMapper::setMouseRelative( float x, float y ){

	int lastX = m_X; 
	int lastY = m_Y; 

	m_X = (int)( x * m_Width );
	m_Y = (int)( y * m_Height );

	int deltaX = m_X - lastX; 
	int deltaY = m_Y - lastY; 

	m_WSClient->sendMessage( "ptm", deltaX, deltaY ); 
} 

void CMouseMapper::setMouseDown( int choice, bool fakeClick ){

	if ( m_PressedButtons[choice] == false ){ // send event only once
		m_PressedButtons[choice] = true; 

		if ( !fakeClick ){ // fake click is used when tracker status is changed
			string button = enumToStrButton( choice );
			m_WSClient->sendMessage( "pointerPress", button ); 
		}
	}
}

void CMouseMapper::setMouseUp( int choice ){

	if ( m_PressedButtons[choice] == true ){
		m_PressedButtons[choice] = false; 

		string button = enumToStrButton( choice );
		m_WSClient->sendMessage( "pointerRelease", button );
	}
}

void CMouseMapper::setScrollingDelta( int wheelDelta ){ 

	wheelDelta = wheelDelta * ( -1 ) * m_ScrollingSpeed; // - scale down, + scale up
	m_WSClient->sendMessage( "pointerScroll", wheelDelta );
}

bool CMouseMapper::isPressed( int choice ){ 
	return m_PressedButtons[choice];
}

