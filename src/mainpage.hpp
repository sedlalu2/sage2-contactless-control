/*!
 *\mainpage SAGE - Contacless handling application
 *\details Application's prototype for bachelor's thesis. 
 *\author Lukáš Sedláček (sedlalu2@fit.cvut.cz)
 *\date 1th March 2015
 *
 *\section Task  
 *- The main task was to made an application which allows users to handle SAGE2 environment contactless by using depth sensors. Any available depth device should 
 work with the application (according to it's drivers avalability). 
 *- The application should communicate with SAGE2 through websockets and messages should be sent as data in JSON format.  
 *- SAGE2 reference: http://sage2.sagecommons.org/
 *
 *\section Basics 
 *\subsection Features 
 *- The application is based on: 
 *  - OpenNI2 library for device initialization (http://structure.io/openni)
 *  - easywsclient library for websocket communication with SAGE2 websocket server (https://github.com/dhbaird/easywsclient)
 *  - NiTE2 library for hand tracker initialization
 *  .
 *- The application allows you to use any available depth device. 
 *- User could use both hands for gestures. 
 *- Available gestures are (according to SAGE2 environment specification)
 *  - Mouse movements (mapped on hand's movements)
 *  - Click gestures (left, right, middle)
 *  - Scroll gestures
 *  .
 *- The application uses configuration file. When this file is broken then default values are used.
 * 
 *\subsection Installation
 *- If you are using any other profile definitions file then bashrc, change it. 
 *- Needed libraries
 *  - g++
 *  - libusb-dev
 *  - libusb-1.0-0-dev
 *  - doxygen and graphviz (in case you want to generate documentation)
 *  .
 *- NiTE2 library is needed to download and put into lib folder
 *	- If NiTE2 library's name is not begin with "NiTE", rename it.
 *	- Copy all from NiTE's "Redist" folder to project's bin folder
 *	- cd to project's bin/NiTE folder. In file HandAlgorithms.ini change AllowMultipleHands to 1
 *- Install libraries:
 *	- Library installation
 *		- run as root: sudo ./install
 *		- cat ContactlessSAGE_Environment >> ~/.bashrc
 *  .
 *- Building and run: 
 *	- make 
 *	- make run
 *		- Add entry to you LD_LIBRARY_PATH variable if it is needed
 *			- CONTACTLESS_APP=/path/to/contaclessApp/bin
 *			- echo "export LD_LIBRARY_PATH=$CONTACTLESS_APP:$LD_LIBRARY_PATH" >> ~/.bashrc	
 *		.
 *  . 
 *- Other options
 *  - make docs (documentation)
 *  - make compile (only compilation is performed)
 *  - make clean (all object files, binaries and documentation's files are deleted)
 *  - make count (*.cpp and *.h files lines are counted)
 *  - make valgrind (binary with valgrind)
 *  . 
 *  
 *\section GESTURES
 * \image html gestures.png
 */