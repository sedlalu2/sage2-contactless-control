//----------------------------------------------------------------------------------------
/**
 * \file       CTracker.h
 * \author     Lukas Sedlacek (sedlalu2)
 * \date       2015/02/25
 * \brief      Main hand tracker class based on NiTE2 HandTracker class    
*/
//----------------------------------------------------------------------------------------
#include <iostream>
#include <cmath> 

#include "CTracker.h"
#include "Common.h"

using namespace std;

bool CTracker::inTrackingRange(  const nite::Point3f & position ) const {
	return ( ( position.z > m_Sensitivity ) && ( position.z < ( m_Sensitivity + m_TrackingRange ) ) ); 
} 

const float CTracker::getHandDistance( void ) const { 

	nite::Point3f mouse 	= m_HandsList.m_Mouse->getCurrentHandPosition(); 
	nite::Point3f gesture  	= m_HandsList.m_Gesture->getCurrentHandPosition(); 

	float x = ( mouse.x - gesture.x ); 
	float y = ( mouse.y - gesture.y ); 
	float z = ( mouse.z - gesture.z ); 

	float powDistance = x*x + y*y + z*z; 
	return sqrt( powDistance ); 
}

void CTracker::updateTrackerState( void ){ 

	if ( ( m_HandsList.m_Mouse && m_HandsList.m_Gesture ) ) { 
	
		if ( ( m_MouseMapper.isPressed( LEFT_BUTTON ) ) && 
			 ( m_HandsList.m_Mouse->checkForClickGesture( LR_CLICK_GESTURE ) ) ){
			
			m_MouseMapper.setMouseUp( LEFT_BUTTON ); 				
			m_MouseMapper.startScrolling(); 
			m_HandDistance = this->getHandDistance(); 
			m_State = SCROLLING_STATE; 
		}

		if ( (!m_HandsList.m_Mouse->checkForClickGesture( LR_CLICK_GESTURE ) ||  
			  !m_HandsList.m_Gesture->checkForClickGesture( LR_CLICK_GESTURE ) ) && (m_State == SCROLLING_STATE) ){
			
			m_State = MOUSE_TRACKING_STATE; 			
			this->updateHandBox(); 

			if ( !m_HandsList.m_Gesture->checkForClickGesture( LR_CLICK_GESTURE ) ){
				m_MouseMapper.setMouseDown( RIGHT_BUTTON, true ); 
			}
		} 
	} else { 
		m_State = MOUSE_TRACKING_STATE; 
	}
}

void CTracker::processNewHand( nite::HandData * hand ){

	const nite::Point3f position3D = hand->getPosition();
	const nite::HandId  handId = hand->getId(); 
	
	CHand * tempHand = new CHand( m_ClickDistance );
	tempHand->setHandPosition( position3D, true ); 
	tempHand->setId( handId ); 

	if ( m_HandsList.m_Mouse == NULL ){

		m_HandsList.m_Mouse = tempHand; 
		m_HandsList.m_Mouse->setType( HAND_MOUSE ); 
		this->updateHandBox();

	} else if ( ( m_HandsList.m_Gesture == NULL ) && ( m_HandsList.m_Mouse != NULL ) ){

		m_HandsList.m_Gesture = tempHand; 
		m_HandsList.m_Gesture->setType( HAND_GESTURE ); 
	}
}

void CTracker::processHandsMovements( CHand * activeHand ){

	if ( activeHand->getType() == HAND_MOUSE ){

		float relativeX, relativeY;
		activeHand->getRelativePosition( &relativeX, &relativeY );
		m_MouseMapper.setMouseRelative( relativeX, relativeY ); 	

		this->processClickGesture( activeHand, LR_CLICK_GESTURE, RIGHT_BUTTON ); 		
	}

	if ( activeHand->getType() == HAND_GESTURE ){
		this->processClickGesture( activeHand, LR_CLICK_GESTURE, LEFT_BUTTON ); 
		this->processClickGesture( activeHand, WHEEL_CLICK_GESTURE, MIDDLE_BUTTON ); 		
	}
}

void CTracker::processClickGesture( CHand * hand, int gestureType, int buttonType ){ 
	
	if ( hand->checkForClickGesture( gestureType ) ){
		m_MouseMapper.setMouseDown( buttonType );  
	} else { 
		m_MouseMapper.setMouseUp( buttonType ); 
	}	
}

void CTracker::processScrolling( void ){ 

	float newDistance = this->getHandDistance();	
	float distanceDelta = newDistance - m_HandDistance; 
	m_MouseMapper.setScrollingDelta( distanceDelta );

	m_HandDistance = newDistance;  
}

CHand * CTracker::getActiveHand( nite::HandData * hand ) const { 
	CHand * tmp = NULL; 

	if ( m_HandsList.m_Mouse && ( m_HandsList.m_Mouse->getId() == hand->getId() ) )
		tmp = m_HandsList.m_Mouse; 
	
	if ( m_HandsList.m_Gesture && ( m_HandsList.m_Gesture->getId() == hand->getId() ) )
		tmp = m_HandsList.m_Gesture; 

	return tmp;
} 

void CTracker::updateHandBox( void ){
	float relativeX, relativeY; 
	m_MouseMapper.getCurrentMousePositionRelative( &relativeX, &relativeY ); 
	m_HandsList.m_Mouse->setBoundingBox( m_BBoxWidth, m_MouseMapper.getScreenAspectRatio(), relativeX, relativeY );
}

void CTracker::freeHandsList( int handType ){

	if ( m_HandsList.m_Mouse && ( (handType == HANDS_BOTH) || (handType == HAND_MOUSE) ) ){
		delete m_HandsList.m_Mouse; 
		m_HandsList.m_Mouse = NULL; 
	}

	if ( m_HandsList.m_Gesture && ( (handType == HANDS_BOTH) || (handType == HAND_GESTURE) ) ){
		delete m_HandsList.m_Gesture;
		m_HandsList.m_Gesture = NULL; 
	}
}

void CTracker::freeLostedHand( CHand * lostedHand ){ 

	if ( !lostedHand ) return;  

	int handType = lostedHand->getType();
	this->freeHandsList( handType );

	if ( handType == HAND_MOUSE ){
		m_MouseMapper.setMouseUp( RIGHT_BUTTON ); 
	} else if ( handType == HAND_GESTURE ){
		m_MouseMapper.setMouseUp( LEFT_BUTTON );
	}
}

CTracker::CTracker( void ){
	m_HandsList.m_Mouse = NULL; 
	m_HandsList.m_Gesture = NULL; 
	m_CommonData = NULL; 
	m_State = MOUSE_TRACKING_STATE; 
	m_HandDistance = 0; 
}

CTracker::~CTracker( void ){
	this->freeHandsList( HANDS_BOTH ); 
}

bool CTracker::setDevice( openni::Device * device ){
	m_HandTracker.create( device ); 
	return m_HandTracker.isValid(); 
} 

void CTracker::initializeHandMapping( CSageNetClient * wsClient, CCommonData * common ){ 
	m_CommonData = common;
	m_MouseMapper.initialize( wsClient, m_CommonData );
	
	float smoothness 	= m_CommonData->getNumberValue( "HandTracker", "Smoothness" );
	m_BBoxWidth			= m_CommonData->getNumberValue( "HandTracker", "BoundingBoxWidth" ); 
	m_ClickDistance		= m_CommonData->getNumberValue( "HandTracker", "ClickDistance" ); 	
	m_Sensitivity		= m_CommonData->getNumberValue( "HandTracker", "Sensitivity" ); 
	m_TrackingRange 	= m_CommonData->getNumberValue( "HandTracker", "Range" );	
	
	m_HandTracker.setSmoothingFactor( smoothness ); 
}

void CTracker::start( void ){
	m_HandTracker.startGestureDetection( nite::GESTURE_HAND_RAISE );
	m_MouseMapper.startSagePointer(); 
}

void CTracker::stop( void ){
	m_HandTracker.stopGestureDetection( nite::GESTURE_HAND_RAISE ); 
	m_MouseMapper.stopSagePointer(); 
}

bool CTracker::readFrame( void ){
	
	nite::Status stat = m_HandTracker.readFrame( &m_Frame );  	
	
	if ( stat != nite::STATUS_OK ){
		printToConsole( MESSAGE_EXC_FRAME_SKIP ); 
		return false;  
	}
	return true; 
} 

void CTracker::releaseFrame( void ){ 
	m_Frame.release(); 
}

void CTracker::handDetection( void ){

	const nite::Array<nite::GestureData> & gestures = m_Frame.getGestures(); 
	
	for ( int i = 0; i < gestures.getSize(); i++ ){
		if ( gestures[i].isComplete() ){

			const nite::Point3f position = gestures[i].getCurrentPosition();  

			if ( this->inTrackingRange( position ) ){
				m_HandTracker.startHandTracking( position, &m_HandId ); 
			} else { 
				printToConsole( MESSAGE_TRACKER_OUT ); 
			}
		} 
	}
}

void CTracker::handMapping( void ){

	const nite::Array<nite::HandData> & hands = m_Frame.getHands();

	for ( int i = 0; i < hands.getSize(); i++ ){

		nite::HandData hand = hands[i];

		if ( hand.isNew() && hand.isTracking() ){

			this->processNewHand( &hand ); 

		} else {
			CHand * activeHand = this->getActiveHand( &hand ); 
			if ( !activeHand ) continue; 

			if ( hand.isLost() ){

				this->freeLostedHand( activeHand ); 

			} else { 			

				activeHand->setHandPosition( hand.getPosition() ); 
				
				this->updateTrackerState();

				if ( m_State == MOUSE_TRACKING_STATE ){
					this->processHandsMovements( activeHand );
				} else {
					this->processScrolling(); 
				}
			}
		}
	} 	
}

void CTracker::closeTracker( void ){
	m_HandTracker.destroy(); 
}