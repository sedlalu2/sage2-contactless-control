//----------------------------------------------------------------------------------------
/**
 * \file       CCommonData.cpp
 * \author     Lukas Sedlacek (sedlalu2)
 * \date       2015/02/25
 * \brief      Configuration file loader    
*/
//----------------------------------------------------------------------------------------
#include <algorithm> 
#include <sstream> 
#include <fstream> 

#include "CCommonData.h"
#include "Common.h"

using namespace std; 

CCommonData::CCommonData( void ) { 

	insertIntoMap( "Network", "Address", "127.0.0.1" ); 
	insertIntoMap( "Network", "Port", "8080" ); 
	
	insertIntoMap( "Pointer", "Label", "Default" ); 
	insertIntoMap( "Pointer", "Color", "#B4B4B4" ); 

	insertIntoMap( "HandTracker", "Sensitivity", "1000" ); 
	insertIntoMap( "HandTracker", "Range", "1500" ); 

	insertIntoMap( "HandTracker", "Smoothness", "0.05" ); 
	insertIntoMap( "HandTracker", "BoundingBoxWidth", "500" ); 
	insertIntoMap( "HandTracker", "ClickDistance", "100" ); 

	insertIntoMap( "Screen", "Width", "1920" ); 
	insertIntoMap( "Screen", "Height", "1080" ); 
	insertIntoMap( "Screen", "ScrollingSpeed", "1.0" ); 
}

bool CCommonData::isComment( string & line ){ // checks if line is comment 

	if ( line.empty() || line[0] == ';' ) return true; 

	for ( unsigned int i = 0; i < line.size(); i++ ) { 
		if ( line[i] == ' ' || line[i] == '\t' ) continue; 	// free space at the begining
		else if ( line[i] == ';' ) return true;  // first found symbol 
		else {
			line = line.substr( i ); 
			return false; 
		}
	}
	return false;  
}

bool CCommonData::isHeader( string & line, string & header ){

	line.erase( std::remove( line.begin(), line.end(), ' ' ), line.end() );
	line.erase( std::remove( line.begin(), line.end(), '\t' ), line.end() );

	unsigned size = line.size(); 
	if ( ( line[0] == '[' && line[size-1] != ']' ) || ( line[0] != '[' && line[size-1] == ']' ) ){ 
		printToConsole( MESSAGE_CONFIG_BAD ); 
		header.clear(); 
		return false; 
	} else if ( line[0] != '[' ) { 
		return false;
	}

	line.erase( line.begin() + 0 ); // removes brackets
	line.erase( line.end() - 1 ); 
	header.clear();
	header = line; 
	return true; 
}
// true if address is ok 
bool CCommonData::controlAddress	( const string & address ) const{

	if ( !address.compare( "localhost" ) ) return true; 

	int dotCounter = 0;
	for ( unsigned int i = 0; i < address.length(); i++ ){
		if ( address[i] == '.') {
			dotCounter++;
			continue; 
		}
		else if ( !isdigit( address[i] ) ) return false;
	}	
	if ( dotCounter != 3 ) return false; 
	return true; 
}
// true if value is legal 
bool CCommonData::checkValueLegal( const string & attribute, const string & value ) const{

	float val = isNumber( value );

	if ( !attribute.compare( "Address" ) ){ 
		return this->controlAddress( value ); 
	} else if ( !attribute.compare( "Port" ) ) { // port 
		float port = isNumber( value, true ); 
		return ( port >= 0 ); 
	} else if ( !attribute.compare( "Smoothness" ) ) {
		return ( val >= 0 && val <= 1 ); 
	} else if ( !attribute.compare( "Color" ) ) { 
		return ( (value[0] == '#') && ( value.length() == 7 ) );
	} else if ( !attribute.compare( "Label" ) ) 
		return true; 
	else { 
		return ( val >= 0 ); 
	}

	return true; 
}

bool CCommonData::parseLine( string & line, string & attribute, string & value ){ 

	const char delimiter = '=';

	size_t found = line.find( delimiter );
	if ( found == string::npos ) { 
		return false; 
	}

	line.replace( found, 1, 1, ' ' ); // Replace = with ' '
	
	string dump; 
	stringstream iss( line );
	iss >> attribute >> value >> dump; // attribute=value

	if ( attribute.empty() || value.empty() || !dump.empty() ) {
		return false; 
	}
	return true; 
}

// returns iterator to value in multimap. If it is not in multimap then returns end iterator 
mmapit CCommonData::findValueIterator( const string & header, const string & attribute ){ 

	pair <mmapit, mmapit> ret;
	ret = m_Data.equal_range( header );	

	for ( mmapit it = ret.first; it != ret.second; ++it ){
		if ( !it->second.m_Attribute.compare( attribute ) ) { 
			return it; 
		}
	}
	return m_Data.end(); 
}

void  CCommonData::insertIntoMap( const string & header, const string & attribute, const string & value ){
	// check if value is in multimap, if is, change its value  
	mmapit it = findValueIterator( header, attribute );
	if ( it == m_Data.end() ){ 
		iniData valuesToInsert; 
		valuesToInsert.m_Attribute = attribute; 
		valuesToInsert.m_StringVal = value; 
		m_Data.insert( pair< string, iniData >( header, valuesToInsert ) );
	} else { 
		it->second.m_StringVal = value; 
	}
}

const string CCommonData::findValue( const string & header, const string & attribute ){
	string value; 
	mmapit it = this->findValueIterator( header, attribute );
	if ( it != m_Data.end() ){
		value = it->second.m_StringVal; 
	} else { 
		value = ""; 
	}
	return value; 
}

void CCommonData::loadData( const char * filepath ){

	ifstream in; 
	in.open( filepath ); 
	if ( in.fail() ){
		printToConsole( MESSAGE_CONFIG_LOAD ); 
		return; 
	}

	string line;
	string header;  
	while( getline( in, line ) ) { 

		if ( this->isComment( line ) ) continue; // or empty line
		if ( this->isHeader( line, header ) ) continue; 

		string attribute; 
		string value; 		
		if ( !this->parseLine( line, attribute, value ) ) { 
			printToConsole( MESSAGE_CONFIG_BAD ); 
			continue;
		}
		if ( !this->checkValueLegal( attribute, value ) ) {
			printToConsole( MESSAGE_CONFIG_VALUES ); 
		 	continue; 
		 }

		if ( !header.empty() ){
			this->insertIntoMap( header, attribute, value ); 
		}
	}

	in.close(); 
}

const string CCommonData::getStringValue( const string & header, const string & attribute ){
	return findValue( header, attribute ); 
}

const float CCommonData::getNumberValue( const string & header, const string & attribute ){
	return toNumber( findValue( header, attribute ) ); 
}
