#!/bin/sh

if [ `whoami` != root ]; then
    echo Run this script as ROOT.
    exit
fi

APP_PATH=`pwd`
OUT_FILE="$APP_PATH/ContactlessSAGE_Environment"

#OPENNI
cd lib/OpenNI*
sh ./install.sh
cat OpenNIDevEnvironment > $OUT_FILE
cd $APP_PATH

#NITE
cd lib/NiTE*
sh ./install.sh
cat NiTEDevEnvironment >> $OUT_FILE
cd $APP_PATH 

#EASYWSCLIENT
cd lib/easywsclient/Include
WS_CLIENT=`pwd`
cd $APP_PATH 
echo "export WSCLIENT_INCLUDE=$WS_CLIENT" >> $OUT_FILE

chmod a+r $OUT_FILE