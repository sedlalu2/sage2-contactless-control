#author Lukas Sedlacek
#sedlalu2@fit.cvut.cz

#variables ------------------------------------------------------------------------------
CXX=g++
CLIBS=-L./bin -lOpenNI2 -lNiTE2 -lwsclient
CFLAGS=-Wall -pedantic -Wno-long-long -O0 -ggdb -I$(OPENNI2_INCLUDE) -I$(NITE2_INCLUDE) -I$(WSCLIENT_INCLUDE)
SRCDIR=src
BIN=contactlessSage
BINPATH=./bin/$(BIN)
DOCPATH=./doc/html
RM=rm -Rf

OBJECTS=\
	$(SRCDIR)/main.o\
	$(SRCDIR)/CContactlessApp.o\
	$(SRCDIR)/CException.o\
	$(SRCDIR)/CTracker.o\
	$(SRCDIR)/Common.o\
	$(SRCDIR)/CHand.o\
	$(SRCDIR)/CMouseMapper.o\
	$(SRCDIR)/CSageNetClient.o\
	$(SRCDIR)/CCommonData.o\
	$(SRCDIR)/TBoundingBox.o

#Makefile arguments ----------------------------------------------------------------------

#All
all: compile docs

#Binary
compile: $(BINPATH)

#Clean all created files 
clean: 
	$(RM) $(OBJECTS) $(BINPATH) $(DOCPATH)

#Run with valgrind
valgrind: 
	cd ./bin; valgrind --leak-check=full ./$(BIN); cd .. 	

#Generate documentation
docs: 
	doxygen

#Run binary 	
run: 
	cd ./bin; ./$(BIN) -f ../config/config.ini; cd .. 
#Count lines 
count: 
	wc -l src/*.h src/*.cpp src/*.hpp
#rules ----------------------------------------------------------------------------------

$(BINPATH): $(OBJECTS)
	$(CXX) $(CFLAGS) $(OBJECTS) $(CLIBS) -o $(BINPATH)

$(SRCDIR)/main.o: $(SRCDIR)/main.cpp
	$(CXX) $(CFLAGS) $(CLIBS) -c $(SRCDIR)/main.cpp -o $(SRCDIR)/main.o

$(SRCDIR)/CContactlessApp.o: $(SRCDIR)/CContactlessApp.cpp
	$(CXX) $(CFLAGS) $(CLIBS) -c $(SRCDIR)/CContactlessApp.cpp -o $(SRCDIR)/CContactlessApp.o

$(SRCDIR)/CException.o: $(SRCDIR)/CException.cpp
	$(CXX) $(CFLAGS) $(CLIBS) -c $(SRCDIR)/CException.cpp -o $(SRCDIR)/CException.o

$(SRCDIR)/CTracker.o: $(SRCDIR)/CTracker.cpp
	$(CXX) $(CFLAGS) $(CLIBS) -c $(SRCDIR)/CTracker.cpp -o $(SRCDIR)/CTracker.o

$(SRCDIR)/Common.o: $(SRCDIR)/Common.cpp
	$(CXX) $(CFLAGS) $(CLIBS) -c $(SRCDIR)/Common.cpp -o $(SRCDIR)/Common.o

$(SRCDIR)/CHand.o: $(SRCDIR)/CHand.cpp
	$(CXX) $(CFLAGS) $(CLIBS) -c $(SRCDIR)/CHand.cpp -o $(SRCDIR)/CHand.o

$(SRCDIR)/CMouseMapper.o: $(SRCDIR)/CMouseMapper.cpp
	$(CXX) $(CFLAGS) $(CLIBS) -c $(SRCDIR)/CMouseMapper.cpp -o $(SRCDIR)/CMouseMapper.o

$(SRCDIR)/CSageNetClient.o: $(SRCDIR)/CSageNetClient.cpp
	$(CXX) $(CFLAGS) $(CLIBS) -c $(SRCDIR)/CSageNetClient.cpp -o $(SRCDIR)/CSageNetClient.o

$(SRCDIR)/CCommonData.o: $(SRCDIR)/CCommonData.cpp
	$(CXX) $(CFLAGS) $(CLIBS) -c $(SRCDIR)/CCommonData.cpp -o $(SRCDIR)/CCommonData.o

$(SRCDIR)/TBoundingBox.o: $(SRCDIR)/TBoundingBox.cpp
	$(CXX) $(CFLAGS) $(CLIBS) -c $(SRCDIR)/TBoundingBox.cpp -o $(SRCDIR)/TBoundingBox.o
