SAGE2 contacless control application 
============

[SAGE2](http://sage2.sagecommons.org/) contacless control application is an application 
to get SAGE2 environment controlled by hand gestures. The application supports devices with 
depth sensor (all tests were perfomed with Microsoft's Kinect device). A communication with 
SAGE2 is conveyed throught websockets. 

Dependencies
=====

The application is based on this libraries: 

* [OpenNI2.X](http://structure.io/openni)
* [easywsclient](https://github.com/dhbaird/easywsclient)
* NiTE2.X by PrimeSense company - not included in impelementation - has to be downloaded separately

Instalation
===== 
* BEWARE: If you are using any other profile definitions file then bashrc, change it in another description. 
* Needed libraries: 
	* g++
	* libusb-dev 	
	* libusb-1.0-0-dev
	* doxygen and graphviz (in case you want to generate documentation)
*  NiTE2 library is needed to download and put into lib folder
	* If NiTE2 library's name is not begin with "NiTE", rename it.
	* Copy all from NiTE's "Redist" folder to project's bin folder
	* cd to project's bin/NiTE folder. In file HandAlgorithms.ini change AllowMultipleHands to 1
*  Install libraries
	* run as root: sudo ./install
	* cat ContactlessSAGE_Environment >> ~/.bashrc 
*  Building and run: 
	* make 
	* make run
	* Add entry to you LD_LIBRARY_PATH variable if it is needed
		* CONTACTLESS_APP=/path/to/contaclessApp/bin
		* echo "export LD_LIBRARY_PATH=$CONTACTLESS_APP:$LD_LIBRARY_PATH" >> ~/.bashrc
